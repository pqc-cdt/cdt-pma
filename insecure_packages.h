#ifdef INSECURE_PACKAGES_H
#define INSECURE_PACKAGES_H

void get_insecure_package_db(char*** insecure_data, int* insecure_data_size);

#endif