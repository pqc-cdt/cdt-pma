#include "insecure_packages.h"


void get_insecure_package_db(char ***insecure_data, int *insecure_data_size) {
    // TODO: Obviously this logic can be improved massively, by taking
    //       specific versions/version-ranges into account etc. Packages get also fixed etc.
    static char *insecure_packages[] = {"openssl",
                                        "libcryptsetup12:amd64",
                                        "gzip",
                                        "libssh-gcrypt-4:amd64",
                                        "libgssapi-krb5-2",
                                        "libcrypt1:amd64"};
    *insecure_data = insecure_packages;
    *insecure_data_size = sizeof(insecure_packages) / sizeof(insecure_packages[0]);
}
