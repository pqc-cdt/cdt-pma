#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/utsname.h>

char *run_external_command(const char *command_string, int *exit_code) {
// Create a new stream connected to a pipe running the given command.
    FILE *fp = popen(command_string, "r");
    if (fp == NULL) {
        return NULL;
    }

    size_t bytes_read = 0;
    size_t buffer_size = 0;
    size_t buffer_offset = 0;
    size_t total_bytes_read = 0;
    char *buffer = NULL;

    while (1) {
        // Grow buffer to hold output of external command
        buffer = (char *) realloc((void *) buffer, buffer_size + 64);
        if (!buffer) {
            pclose(fp);
            return NULL;
        }
        buffer_size += 64;

        // Read the output of the external command into the buffer
        bytes_read = fread(buffer + buffer_offset, sizeof(char), buffer_size - buffer_offset, fp);
        total_bytes_read += bytes_read;
        if (bytes_read == 0) {
            break;
        }

        // Update buffer offset
        buffer_offset += 64;
    }

    buffer[total_bytes_read] = '\0';
    int res = pclose(fp);
    *exit_code = WEXITSTATUS(res);

    return buffer;
}

int count_occurrences(const char *input, char c) {
    int occurrences = 0;
    for (int i = 0; input[i] != '\0'; i++)
        if (input[i] == c)
            occurrences++;
    return occurrences;
}

char* concat_comma_list(char* list, char* value) {
    if (list == NULL) {
        // Allocate memory for the initial list
        list = (char*)malloc((strlen(value) + 1) * sizeof(char));
        strcpy(list, value);
    } else {
        // Reallocate memory to accommodate the comma and new value
        list = (char*)realloc(list, (strlen(list) + strlen(value) + 2) * sizeof(char));
        strcat(list, ",");
        strcat(list, value);
    }

    return list;
}

char **str_split(char *str, const char *delim, size_t *output_count) {
    size_t arr_size = 2;
    size_t arr_idx = 0;
    char **out_arr;
    char **realloc_buf;
    char *strtok_ptr;

    // Allocate space for split string array
    if (!(out_arr = malloc(arr_size * sizeof(*out_arr)))) {
        return NULL;
    }

    // Populate first index of array before loop to avoid seg fault
    out_arr[arr_idx] = strtok_r(str, delim, &strtok_ptr);

    // Split until end of string
    while (out_arr[arr_idx] != NULL) {
        arr_idx++;
        // Resize out_arr if necessary
        if (arr_idx >= arr_size) {
            arr_size = arr_size << 1;
            if (!(realloc_buf = realloc(out_arr, (arr_size * sizeof(*out_arr))))) {
                free(out_arr);
                return NULL;
            }
            out_arr = realloc_buf;
        }
        out_arr[arr_idx] = strtok_r(NULL, delim, &strtok_ptr);
    }

    *output_count = arr_idx;
    return out_arr;
}

void get_hostname(char* hostname, int bufferSize) {
    if (gethostname(hostname, bufferSize) != 0) {
        perror("Error retrieving hostname");
    }
}

void get_kernel_version(char* version, int bufferSize) {
    struct utsname buffer;

    if (uname(&buffer) != 0) {
        perror("Error retrieving kernel version");
        return;
    }
    snprintf(version, bufferSize, "%s", buffer.release);
}
