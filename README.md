# cdt-pma

## Introduction

Quantum computers are a rapidly advancing area of research with the potential to revolutionize computing as we know it. 
However, their ability to quickly solve certain types of mathematical problems could also pose a significant threat to current cryptographic systems.
The Crypto-Detection-Tool project aims to provide a comprehensive suite of tools designed to assist system administrators in evaluating the resilience of the cryptographic components installed on a Linux system against potential threats posed by post-quantum computers

## Summary

This tool, cdt-pma, is part of the crypto-detection-tool project.
It will query all the installed packages on the system, and compares them internally against a list of known insecure packages.
The findings will be reported, including possible dependencies of the flagged packages, since they might be affected as well.
As of now, only the apt package manager is supported.

## Features

Currently, cdt-pma provides the following features:

- Find insecure packages, installed on the system.
- Recursively find any packages that depend on insecure packages, and therefore might also be affected
- Classify all findings using a (simple) severity metric
- Obtain all the insecure dependencies
- Extract package specific information (package version)
- Extract a general information (scan times, hostname, kernel version, total package count, affected package count etc)

## How does it work?

1. The tool obtains a list of all installed packages + version, by interacting with `dpkg`.
2. All these packages are then compared against an internal knowledge base of pq-insecure packages (currently just some dummy packages)
3. The insecure packages are flagged as high severity, because they are considered "directly insecure"
4. The tool iterates through all installed packages and interacts with the `apt-rdepends` tool obtain a full recursive dependency list for each package. All  dependencies of each package is then checked, if there is a match in the knowledge base. If so the package is considered insecure (low), because, even if the package itself is not insecure, it utilizes an insecure package in its dependency chain. If there are multiple insecure packages in the chain, it is classified as medium severity.
5. All the findings are then exported in a report, together with some system information.

The severity metric is explained again below:

| Severity | Package  | Insecure Dependencies |
|----------|----------|-----------------------|
| High     | Insecure | 0 or more             |
| Medium   | Secure   | 2 or more             |
| Low      | Secure   | 1                     |


## Future work

The metric to evaluate can be improved a lot - the current one just serves as a simple base line.
It is possible to include additional heursitics such as:
- When was the last update of the package
- How often is the package updated
- Does the package have many / few cryto syscalls
- Does the package have CVEs that address cryptographic issues

The list is endless...


## Dependencies:

- [json-c](https://github.com/json-c/json-c)
- apt-rdepends
- dpkg