#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdarg.h>

/**
 * Formats a command string, similar to printf, and runs it as an external process.
 * The stdout of the external process is returned.
 * @param format_string The format command string
 * @param ... All additional parameters that will be combined with the format string
 * @return The stdout after running the format string as external process.
 */
//char *run_external_command(const char *format_string, ...);
char *run_external_command(const char *command_string, int *exit_code);

/**
 * Returns how many times a character occurs in a given string
 * @param input The input string
 * @param c The character to count
 * @return The occurrence count
 */
int count_occurrences(const char *input, char c);

/**
 * Splits a string at a given delimiter and returns the splitted segments
 * @param str The input string to split
 * @param delim The delimiter at which the input string gets splitted
 * @param output_count The amount of splitted segments
 * @return An array of all segments
 */
char **str_split(char *str, const char *delim, size_t *output_count);

char* concat_comma_list(char* list, char* value);

void get_hostname(char* hostname, int bufferSize);

void get_kernel_version(char* version, int bufferSize);

#endif