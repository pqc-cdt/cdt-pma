all:
	gcc -Wall -Wextra -pedantic -g \
	main.c \
	analyzer.c \
	package.c \
	insecure_packages.c \
	utils.c \
	-o cdt-pma \
	-ljson-c \
	#-lasan
