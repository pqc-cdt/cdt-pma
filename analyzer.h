#ifndef ANALYZER_H
#define ANALYZER_H

#include "package.h"
#include <json-c/json.h>

typedef enum linux_distribution {
    DEBIAN,
    ARCH
} linux_distribution;

typedef struct package_manager {
    linux_distribution distribution;
    int all_packages_count;
    int insecure_pkgs_count;
    int pkgs_with_insecure_deps_count;
    package **all_packages;
    json_object* report;
} package_manager;

package_manager *analyzer_init();

void query_packages_apt(package_manager *pkgm);

void flag_insecure_packages(package_manager *pkgm);

void flag_insecure_dependencies(package_manager *pkgm);

package *find_package_by_name(package_manager *pkgm, char *pkg_name);

void lookup_reverse_dependencies(package_manager *pkgm, package *package);

void export_report(package_manager *pkgm);

void analyzer_free(package_manager *pkgm);

#endif