#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "analyzer.h"
#include "utils.h"
#include "insecure_packages.h"
#include "package.h"
#include <time.h>
#include <sys/utsname.h>

package_manager *analyzer_init() {
    package_manager *pkgm = malloc(sizeof(package_manager));
    if (!pkgm) { exit(1); }

    pkgm->distribution = DEBIAN;
    pkgm->all_packages_count = 0;
    pkgm->pkgs_with_insecure_deps_count = 0;
    pkgm->insecure_pkgs_count = 0;

    pkgm->report = json_object_new_object();

    printf("[+] Initializing package scan ...\n");
    time_t currentTime = time(NULL);
    char time_buf[100];
    strftime(time_buf, sizeof(time_buf), "%Y-%m-%d %H:%M:%S", localtime(&currentTime));
    json_object_object_add(pkgm->report, "pm_scan_start", json_object_new_string(time_buf));

    query_packages_apt(pkgm);

    return pkgm;
}

void query_packages_apt(package_manager *pkgm) {

    printf("[+] Querying all installed packages ...\n");

    // Query raw list of installed packages
    int exit_code;
    char *raw_output = run_external_command("dpkg-query -W -f='${binary:package};${Version}\n'", &exit_code);
    if (exit_code != 0 || !raw_output) {
        printf("[!] Failed to query installed packages from apt\n");
        exit(1);
    }

    // Parse the output
    char *line;
    char *field;
    char *line_ptr;
    char *field_ptr;
    line = strtok_r(raw_output, "\n", &line_ptr);
    while (line != NULL) {
        field = strtok_r(line, ";", &field_ptr);
        while (field != NULL) {
            // Extract all values from the CSV output
            package *pkg = (package *) calloc(1, sizeof(package));
            pkg->name = strdup(field);
            field = strtok_r(NULL, ";", &field_ptr);
            pkg->version = strdup(field);
            field = strtok_r(NULL, ";", &field_ptr);

            // Resize all_packages, if necessary
            if (!pkgm->all_packages) {
                pkgm->all_packages = calloc(1, sizeof(package));
            } else {
                pkgm->all_packages = realloc(pkgm->all_packages, sizeof(package) * (pkgm->all_packages_count + 1));
            }
            pkgm->all_packages[pkgm->all_packages_count] = pkg;
            pkgm->all_packages_count++;
        }
        line = strtok_r(NULL, "\n", &line_ptr);
    }
    printf("[+] Detected %d installed packages\n", pkgm->all_packages_count);
}

void flag_insecure_packages(package_manager *pkgm) {
    printf("[+] Checking for insecure packages ...\n");

    // Obtain knowledge base of insecure packages
    char **insecure_data = NULL;
    int insecure_data_size = 0;
    get_insecure_package_db(&insecure_data, &insecure_data_size);

    // Iterate over all packages, and flag those that are insecure
    for (int i = 0; i < pkgm->all_packages_count; ++i) {
        // For each package, check if its part of the insecure_package knowledge base
        for (int j = 0; j < insecure_data_size; ++j) {
            if (strcmp(pkgm->all_packages[i]->name, insecure_data[j]) == 0) {
                printf("[!] Package: %s is known to be insecure.\n", pkgm->all_packages[i]->name);
                pkgm->all_packages[i]->is_insecure = true;
                pkgm->all_packages[i]->severity = HIGH;
                pkgm->insecure_pkgs_count++;
                break;
            }
        }
    }
}

void flag_insecure_dependencies(package_manager *pkgm) {

    printf("[+] Checking packages with insecure dependencies ...\n");
    // Iterate over all packages, and flag those, that depend on the insecure ones
    for (int i = 0; i < pkgm->all_packages_count; ++i) {
        // Check if the packages that are not considered insecure depend on an insecure package
        //if (pkgm->all_packages[i]->is_insecure == false) {
        lookup_reverse_dependencies(pkgm, pkgm->all_packages[i]);
        printf("%d/%d\n", i, pkgm->all_packages_count);
        //}
    }
}

void lookup_reverse_dependencies(package_manager *pkgm, package *package) {

    // Construct the command string
    char *cmd_buf;
    asprintf(&cmd_buf, "apt-rdepends -s=DEPENDS --state-follow=Installed --state-show=Installed -r %s", package->name);

    // Execute command to get reverse deps
    int exit_code;
    char *raw_output = run_external_command(cmd_buf, &exit_code);
    if (exit_code != 0) {
        printf("[-] Failed to query reverse dependencies for package %s\n", package->name);
        exit(1);
    }

    // Format list of reverse dependencies
    char *dep_pkg;
    dep_pkg = strtok(raw_output, "\n");

    int insecure_dep_count = 0;
    while (dep_pkg != NULL) {
        // Try to resolve package deps by name
        struct package *p = find_package_by_name(pkgm, dep_pkg);
        if (p != NULL) {
            // Check each dependency if it was previously marked as is_insecure
            for (int i = 0; i < pkgm->all_packages_count; ++i) {
                if (strncmp(pkgm->all_packages[i]->name, p->name, strlen(p->name)) == 0 &&
                    pkgm->all_packages[i]->is_insecure == true) {

                    // Make sure we package does not classify itself as its own dependency
                    if (strncmp(package->name, pkgm->all_packages[i]->name, strlen(package->name)) == 0){
                        continue;
                    }

                    printf("Package %s depends on insecure package %s\n", package->name, p->name);
                    package->has_insecure_deps = true;

                    // If we have a single insecure dependency -> severity = low
                    // If we have a multiple insecure dependencies -> severity = medium
                    // Note: Of course this evaluation metric is extremely simple and should be replaced
                    // with a better heuristic in the future!
                    if (insecure_dep_count == 0) {
                        package->severity = LOW;
                    } else {
                        if (package->is_insecure) {
                            package->severity = HIGH;
                        } else {
                            package->severity = MEDIUM;
                        }
                    }
                    insecure_dep_count++;
                    package->insecure_deps_list = concat_comma_list(package->insecure_deps_list, p->name);

                }
            }
        }
        dep_pkg = strtok(NULL, "\n");
    }
    free(cmd_buf);
}

package *find_package_by_name(package_manager *pkgm, char *pkg_name) {
    if (!pkg_name) { return NULL; }
    for (int i = 0; i < pkgm->all_packages_count; ++i) {
        if (strncmp(pkgm->all_packages[i]->name, pkg_name, strlen(pkg_name)) == 0) {
            return pkgm->all_packages[i];
        }
    }
    return NULL;
}

void export_report(package_manager *pkgm) {
    // Store time scan ended
    time_t currentTime = time(NULL);
    char time_buf[100];
    strftime(time_buf, sizeof(time_buf), "%Y-%m-%d %H:%M:%S", localtime(&currentTime));
    json_object_object_add(pkgm->report, "pm_scan_end", json_object_new_string(time_buf));

    // Store hostname of the system
    char hostname[256];
    get_hostname(hostname, sizeof(hostname));
    json_object_object_add(pkgm->report, "pm_sys_hostname", json_object_new_string(hostname));

    // Store kernel version
    char kernel_ver[256];
    get_kernel_version(kernel_ver, sizeof(kernel_ver));
    json_object_object_add(pkgm->report, "pm_sys_kernel_version", json_object_new_string(kernel_ver));

    // Store total package count
    json_object_object_add(pkgm->report, "pm_sys_total_pkgs", json_object_new_int(pkgm->all_packages_count));

    // Store insecure package count
    json_object_object_add(pkgm->report, "pm_sys_insecure_pkgs", json_object_new_int(pkgm->insecure_pkgs_count));

    // Store count of packages that depend on insecure packages
    for (int i = 0; i < pkgm->all_packages_count; ++i) {
        if (pkgm->all_packages[i]->has_insecure_deps) {
            pkgm->pkgs_with_insecure_deps_count++;
        }
    }
    json_object_object_add(pkgm->report, "pm_sys_insecure_pkg_deps",
                           json_object_new_int(pkgm->pkgs_with_insecure_deps_count));

    // Store information about the analyzed packages
    struct json_object *pkg_arr = json_object_new_array();
    const char *severity_str[] = {"unknown", "low", "medium", "high"};
    for (int i = 0; i < pkgm->all_packages_count; ++i) {
        if (pkgm->all_packages[i]->is_insecure || pkgm->all_packages[i]->has_insecure_deps) {
            package *pkg = pkgm->all_packages[i];
            struct json_object *pkg_obj = json_object_new_object();
            json_object_object_add(pkg_obj, "name", json_object_new_string(pkg->name));
            json_object_object_add(pkg_obj, "version", json_object_new_string(pkg->version));
            json_object_object_add(pkg_obj, "severity", json_object_new_string(severity_str[pkg->severity]));
            json_object_object_add(pkg_obj, "is_insecure", json_object_new_boolean(pkg->is_insecure));
            json_object_object_add(pkg_obj, "has_insecure_deps", json_object_new_boolean(pkg->has_insecure_deps));
            if (!pkg->insecure_deps_list) {
                json_object_object_add(pkg_obj, "insecure_deps_list", json_object_new_string(""));
            } else {
                json_object_object_add(pkg_obj, "insecure_deps_list", json_object_new_string(pkg->insecure_deps_list));
            }
            json_object_array_add(pkg_arr, pkg_obj);
        }
    }
    json_object_object_add(pkgm->report, "packages", pkg_arr);

    // Write report to disk
    const char *json_str = json_object_to_json_string_ext(pkgm->report, JSON_C_TO_STRING_PRETTY);
    FILE *file = fopen("output.pmreport", "w");
    if (file != NULL) {
        fputs(json_str, file);
        fclose(file);
        printf("[+] Report exported successfully!\n");
    } else {
        printf("[-] Failed to export report!\n");
    }
}


void analyzer_free(package_manager *pkgm) {
    free(pkgm);
}