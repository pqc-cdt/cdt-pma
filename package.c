#include "package.h"
#include <stdio.h>

void package_print(package *pkg) {
    if (!pkg) return;
    const char *severity_str[] = {"unknown", "low", "medium", "high"};
    printf("Package name: %s\n", pkg->name);
    printf("Package version: %s\n", pkg->version);
    printf("Package is insecure: %d\n", pkg->is_insecure);
    printf("Package has insecure deps: %d\n", pkg->has_insecure_deps);
    printf("Insecure deps: %s\n", pkg->insecure_deps_list);
    printf("Package severity: %s\n", severity_str[pkg->severity]);
}
