#include "analyzer.h"

int main(int argc, char **argv) {
    package_manager *pkgm = analyzer_init();
    flag_insecure_packages(pkgm);
    flag_insecure_dependencies(pkgm);
    export_report(pkgm);
    analyzer_free(pkgm);
    return 0;
}