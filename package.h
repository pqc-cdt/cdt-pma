#ifndef PACKAGE_H
#define PACKAGE_H

#include <stdbool.h>

typedef enum severity {
    UNKNOWN = 0,
    LOW = 1,
    MEDIUM = 2,
    HIGH = 3
} severity;

typedef struct package {
    char *name;
    char *version;
    bool is_insecure;
    bool has_insecure_deps;
    char* insecure_deps_list;
    severity severity;
} package;

void package_print(package *pkg);

#endif